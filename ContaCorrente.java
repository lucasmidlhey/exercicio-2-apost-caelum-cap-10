package cap10ex2;

public class ContaCorrente extends Conta implements Tributavel {
	
	public double calculaTributos() {
		return this.getSaldo() * 0.01;
	}

}
